package im.gitter.gitter.utils;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;

public class CursorUtils {
    public static ContentValues getContentValues(Cursor cursor) {
        ContentValues contentValues = new ContentValues();
        DatabaseUtils.cursorRowToContentValues(cursor, contentValues);
        return contentValues;
    }

    public static int getInt(Cursor cursor, String columnName) {
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }
}
