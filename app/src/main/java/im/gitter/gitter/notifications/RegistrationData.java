package im.gitter.gitter.notifications;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class RegistrationData {

    private final SharedPreferences prefs;

    public RegistrationData(Context context) {
        this.prefs = context.getSharedPreferences("gitter_gcm_prefs", Activity.MODE_PRIVATE);

        // delete after 1/1/2016
        transitionLegacyPerfs(context);
    }

    public void setHasRegistered() {
        prefs.edit().putBoolean("has_registered", true).apply();
    }

    public void wipe() {
        prefs.edit().clear().apply();
    }

    public boolean hasRegistered() {
        return prefs.getBoolean("has_registered", false);
    }

    private void transitionLegacyPerfs(Context context) {
        SharedPreferences legacyPrefs = context.getSharedPreferences("StartActivity", Context.MODE_PRIVATE);
        String token = legacyPrefs.getString("gcm_registration_id", null);

        if (token != null) {
            setHasRegistered();
            legacyPrefs.edit().remove("gcm_registration_id").apply();
        }
    }
}
