Gitter是开发商前来洽谈的地方。
We provide free public chat rooms for developer communities and open source projects, as well as private chat rooms for technical teams and businesses.
我们为开发者社区和开源项目提供免费的公共聊天室，为技术团队和企业提供私人聊天室。

KEY FEATURES
主要特征
Unlimited public chat rooms for free
免费无限公共聊天室
Unlimited and searchable chat history
无限和可搜索聊天记录
Unlimited integrations
无限整合
Built on top of GitHub, the world’s largest network of software developers
基于世界最大的软件开发商GitHub所构建
Private chat rooms free up to 25 users
免费私人聊天室可容纳25位用户

TRUSTED BY
取信于
Gitter is home to over 30,000 developer communities, including The .NET Foundation, Google Material Design, Angular.js, Backbone, Node.js, Scala, The W3C and many more.
Gitter是超过30,000个开发者社区的家，其中包括：NET基金会丶谷歌材料设计丶Angular.js丶Backbone丶Node.js丶 Scala和The W3C 等等。

PROBLEMS?
疑问？

FEEDBACK?
反馈？
The more you tell us, the better Gitter gets.
您告诉我们越多，Gitter越能得以改善。
In case of any questions, and in order to find out more about the product, visit our support site: https://gitter.zendesk.com You can also give us direct feedback in the Gitter HQ channel: https://gitter.im/orgs/gitterHQ
如有任何疑问，并了解更多的产品信息，请访问我们的支持网站：https://gitter.zendesk.com 您也可以在Gitter HQ通道直接给我们反馈 https://gitter.im/orgs/gitterHQ
Let us know how we can improve!
让我们知道我们如何改善！
